<?php 
	echo "//Membuat database: CREATE DATABASE JawabanEssayC; <br>";
	echo "//Menggunakan database yang sudah dibuat: USE DATABASE; <br><br>";
	echo "//Membuat table <br>";
	echo "//Table customer <br>";
	echo "CREATE TABLE customer ( <br>";
	echo "-> id int primary key auto_increment <br>
		  -> name varchar (255), <br>
		  -> email varchar (255), <br>
		  -> password varchar (255), <br>
		  ); <br>";
	echo "//Table orders <br>";
	echo "CREATE TABLE orders ( <br>";
	echo "-> id int primary key auto_increment <br>
		  -> amount varchar (255), <br>
		  -> customer_id varchar (255), <br>
		  ); <br><br>";
	echo "//Menambahkan foreign key <br>";
	echo "ALTER TABLE orders ADD FOREIGN KEY (customer_id) REFERENCES customer(id)";


?>